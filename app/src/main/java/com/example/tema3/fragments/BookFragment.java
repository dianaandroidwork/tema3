package com.example.tema3.fragments;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import com.example.tema3.R;
import com.example.tema3.adapters.BookAdapter;
import com.example.tema3.interfaces.ActivitiesFragmentsCommunication;
import com.example.tema3.interfaces.OnBookItemClick;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.example.tema3.models.Book;
import java.util.ArrayList;

public class BookFragment extends Fragment {
    public static final String TAG_BOOK="TAG_BOOK";

    private ActivitiesFragmentsCommunication fragmentCommunication;

    ArrayList<Book> books=new ArrayList<Book>();

    BookAdapter bookAdapter=new BookAdapter(books, new OnBookItemClick() {
        @Override
        public void onClick(Book book) {
            if (fragmentCommunication != null) {
                fragmentCommunication.onReplaceSecondFragment(book);
                Toast.makeText(getContext(), "Go fragment 2", Toast.LENGTH_SHORT).show();
            }
        }
    });

    public static BookFragment newInstance() {
        Bundle args = new Bundle();
        BookFragment fragment = new BookFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.book_fragment,container,false);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        RecyclerView userList = (RecyclerView) view.findViewById(R.id.book_list);
        userList.setLayoutManager(linearLayoutManager);
        books.clear();
        books.add(new Book("1984", "George Orwell","roman"));
        books.add(new Book("Rosu si Negru", "Stendhal","roman psihologic"));
        books.add(new Book("Dama cu camelii", "Alexandre Dumas fiul","roman de dragoste"));
        books.add(new Book("Singur pe lume", "Hector Malot","fictiune"));
        userList.setAdapter(bookAdapter);
        return view;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);

        if(context instanceof ActivitiesFragmentsCommunication){
            fragmentCommunication=(ActivitiesFragmentsCommunication) context;
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        view.findViewById(R.id.button_update).setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Toast.makeText(getContext(),"Click Button",Toast.LENGTH_SHORT).show();
            }
        });
    }
}
