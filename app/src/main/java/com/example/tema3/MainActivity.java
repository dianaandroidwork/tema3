package com.example.tema3;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import android.os.Bundle;
import com.example.tema3.fragments.BookFragment;
import com.example.tema3.fragments.BookDetailsFragment;
import static com.example.tema3.fragments.BookFragment.TAG_BOOK;
import static com.example.tema3.fragments.BookDetailsFragment.TAG_DETAILS;
import com.example.tema3.interfaces.ActivitiesFragmentsCommunication;
import com.example.tema3.models.Book;

public class MainActivity extends AppCompatActivity implements ActivitiesFragmentsCommunication {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        onAddBookFragment();
    }

    private void onAddBookFragment() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.frame_layout, BookFragment.newInstance(), TAG_BOOK);
        fragmentTransaction.commit();
    }

    @Override
    public void onReplaceFragment(String TAG) {

    }

    @Override
    public void onReplaceSecondFragment(Book book) {


        String tag=TAG_DETAILS;
        FragmentManager fragmentManager=getSupportFragmentManager();
        FragmentTransaction transaction= fragmentManager.beginTransaction();
        FragmentTransaction addTransaction= transaction.replace(R.id.frame_layout,
                BookDetailsFragment.newInstance(book),tag);
        addTransaction.addToBackStack(tag);
        addTransaction.commit();
    }
}